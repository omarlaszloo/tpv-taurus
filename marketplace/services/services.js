


var services = angular.module('services', ['ngWebSocket'])


    .factory('services',  function ($http ,$websocket){

        var url =  'http://localhost:8000'
        var Services = {}

        



        function generate_form_data(data) {
            var form_data = new FormData();
            angular.forEach(data, function (value, key) {
                form_data.append(key, value);
            });
            return form_data;
        }

        function _postRequest(path, reqdata) {

            return $http.post(url + path, generate_form_data(reqdata), {
                headers: {'Content-Type': undefined},
                transformRequest: function (reqdata) { return reqdata; }
            });
        }

        Services.postRequest = _postRequest;
        
        Services.userRegister = function (reqdata) { return _postRequest('/create/users/', reqdata); };
        Services.userAddress = function (reqdata){ return _postRequest('/create/address/', reqdata); };
        Services.userInfo = function (reqdata) { return _postRequest('/create/infor-user/', reqdata); };
        Services.signinUser = function(reqdata) { return _postRequest('/signin/', reqdata); };
        Services.addPhone = function(reqdata) { return _postRequest('/add/phone/', reqdata);};

        Services.getPhoneList = function() { return $http.get(url + '/list/phone/'); };
        
        return Services
    })