


var app = angular.module('app',  ['ngRoute', 
                                'app.register',
                                'angular-loading-bar', 
                                'ngAnimate'])


    .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeBar = true;
      }])

    .config(['$routeProvider', function($routeProvider){

        $routeProvider

        .when('/signin', {
            templateUrl: '../views/login/signin.html'
        })

        .when('/step1', {
            templateUrl: '../views/register/step1.html'
        })

        .when('/step2', {
            templateUrl: '../views/register/step2.html'
        })

        .when('/step3', {
            templateUrl: '../views/register/step3.html'
        })

        .when('/felidades', {
            templateUrl: '../views/register/congratuletions.html'
        })

        .when('/dashboard', {
            templateUrl: '../views/dashboard/dashboard.html'
        })

        .otherwise({
            redirect: '/'
        })

    }]);


var appDashboard = angular.module('app.dashboard',  ['ngRoute', 'dashboard'])

    .config(['$routeProvider', function($routeProvider){

        $routeProvider


        .when('/home', {
            templateUrl: '../views_dashboard/home/home.html'
        })

        .when('/dashboard', {
            templateUrl: '../views_dashboard/dashboard/dashboard.html'
        })

        .when('/inventario-menu', {
            templateUrl: '../views_dashboard/inventory/inventory-menu.html'
        })

        .when('/agregar-inventario', {
            templateUrl: '../views_dashboard/inventory/inventory.html'
        })

        .when('/caja', {
            templateUrl: '../views_dashboard/caja/caja.html'
        })

        .when('/agregar-disp', {
            templateUrl: '../views_dashboard/addPhone/addPhone.html'
        })

        .when('/agregar-store', {
            templateUrl: '../views_dashboard/addStore/addStore.html'
        })

        .otherwise({
            redirect: '/'
        })

    }])


    /*

        https://github.com/wilk/ng-websocket
    
        https://github.com/wilk/ng-websocket

        https://disparity.github.io/angular-pusher/
        https://github.com/doowb/angular-pusher
        http://stackoverflow.com/questions/24397370/connection-state-with-doowb-angular-pusher
        http://clintberry.com/2013/angular-js-websocket-service/
        https://blog.pusher.com/making-angular-js-realtime-with-pusher/

        https://github.com/AngularClass/angular-websocket

        http://stackoverflow.com/questions/30337839/websocket-service-update-scope-in-angular-using-ngwebsocket

        https://coderwall.com/p/4kldbq/angularjs-ngwebsocket-mock-feature

        http://stackoverflow.com/questions/26299403/angularjs-and-websockets-beyond

        http://clintberry.com/2013/angular-js-websocket-service/

        https://github.com/AngularClass/angular-websocket


    */