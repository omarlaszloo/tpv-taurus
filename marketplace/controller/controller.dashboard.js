


var app = angular.module('dashboard', ['services'])

    .controller('cajaCtrl', function ($scope){

        $scope.confirm = function() {
            
            swal("Vendiste!", "Gracias por su compra!", "success");
        }
    })


    .controller('addPhoneCtrl', function ($scope, services) {


        $scope.savePhone = function() {
            $scope.data = {
                'imei': $scope.imei,
                'uuid': $scope.uuid,
                'name': $scope.name,
                'employee_position': $scope.employee_position
            }
        

            services.addPhone($scope.data).success(function (data, status){
                if(status == 201){
                    swal("Good job!", "You clicked the button!", "success");
                    $scope.$broadcast('transfer',{'message':'ok'});
                }
            })
        }
     })


    .controller('listPhonetCtrl', function ($scope, services) {

        services.getPhoneList().success(function (data, status) {
            $scope.allListPhone = data
        })
        
        $scope.$on('transfer',function(){
            services.getPhoneList().success(function (data, status) {
                $scope.allListPhone = data
            })
        });

        
    })

    .controller('barCodeScannerCtrl', ['$scope', 'services', '$routeParams', '$websocket', function($scope, services, $routeParams, $websocket){

        
        var dataStream = $websocket();

        

        dataStream.onMessage(function(message) {
        //    console.log('2')
            console.log(message);
        });
        //http://lineadecodigo.com/html5/crear-un-websocket/
    }])