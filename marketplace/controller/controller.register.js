

var app = angular.module('app.register', ['services'])

    .controller('step1Ctrl', function ($scope, services, cfpLoadingBar, $window){ 


        /*
            function para registrar al usuario 
        */
        $scope.register_user = function() {

            $scope.data = {
                'name': $scope.name,
                'last_name': $scope.last_name,
                'user':  $scope.user,
                'email':  $scope.email,
                'password': $scope.password
            }
            
            cfpLoadingBar.start();
            services.userRegister($scope.data).success(function (data, status){
                if(status == 201){
                    window.location = '#step2'
                }
            })
        }

        /*
            funcion para registrar la informacion del usuario 
        */

        $scope.addressUser = function() {
            
            $scope.data = {
                'city': $scope.city,
                'municipality': $scope.municipality,
                'address': $scope.address,
                'cp': $scope.cp,
                'phone': $scope.phone
            }

            cfpLoadingBar.start();
            services.userAddress($scope.data).success(function (data, status){
                if(status == 201){
                    window.location = '#step3'
                }
            })

        }

        /*
            funcion para registrar informacion de la tienda
        */

        $scope.infoShop = function() {

            $scope.data = {
                'deal': $scope.deal,
                'puesto': $scope.puesto,
                'employes': $scope.employes
            }

            cfpLoadingBar.start();
            services.userInfo($scope.data).success(function (data, status){
                if(status == 201){
                    window.location = '/views_dashboard/#/home'
                }
            })

        }
        

        $scope.signin = function() {
            
            $scope.data = {
                'user': $scope.user,
                'password': $scope.password
            }

            cfpLoadingBar.start();
            services.signinUser($scope.data).success(function (data, status){
                if(status == 201) {
                    window.location = '/views_dashboard/#/home'
                }
            })
            
        }
    });





    

    