

from rest_framework import serializers
from app.models import UsersTpv, UserAdress, Userinfo, AddDisp



class UserTpvSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsersTpv
        fields = ('name', 'last_name', 'user', 'email', 'password',)


class UserAddressTpvSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAdress
        fields = ('city', 'municipality', 'address', 'cp', 'phone_contact')


class UserInfoEmployees(serializers.ModelSerializer):
    class Meta:
        model = Userinfo
        fields = ('deal', 'position', 'number_Employees')

class AddDispSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddDisp
        fields = ('imei', 'uuid', 'name', 'employee_position')