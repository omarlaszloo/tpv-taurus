from __future__ import unicode_literals
from django.db import models

# Create your models here.

class UsersTpv(models.Model):

    name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    user = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=100, null=True)
    password = models.CharField(max_length=100, null=True)
    

    class Meta:
        ordering = ('name',)


class UserAdress(models.Model):
    
    city = models.CharField(max_length=100, null=True)
    municipality = models.CharField(max_length=100, null=True)
    address = models.CharField(max_length=100, null=True)
    cp = models.CharField(max_length=100, null=True)
    phone_contact = models.CharField(max_length=100, null=True)

    class Meta:
        ordering = ('city',)


class Userinfo(models.Model):

    deal = models.CharField(max_length=100, null=True)
    position = models.CharField(max_length=100, null=True)
    number_Employees = models.CharField(max_length=100, null=True)

    class Meta:
        ordering = ('deal',)


class AddDisp(models.Model):

    imei = models.CharField(max_length=100, null=True)
    uuid = models.CharField(max_length=100, null=True)
    name = models.CharField(max_length=100, null=True)
    employee_position = models.CharField(max_length=100, null=True)


    class Meta:
        ordering = ('imei',)