from django.contrib import admin
from app.models import UsersTpv, UserAdress, Userinfo, AddDisp


# Register your models here.

class UserTpv(admin.ModelAdmin):
    pass

admin.site.register(UsersTpv, UserTpv)

class UserAddress(admin.ModelAdmin):
    pass

admin.site.register(UserAdress, UserAddress)

class UserInfo(admin.ModelAdmin):
    pass

admin.site.register(Userinfo, UserInfo)

class AddDispUser(admin.ModelAdmin):
    pass

admin.site.register(AddDisp, AddDispUser)

