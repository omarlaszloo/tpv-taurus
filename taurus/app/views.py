
import pprint
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app.models import UsersTpv, AddDisp
from app.serializers import UserTpvSerializer, UserAddressTpvSerializer, UserInfoEmployees, AddDispSerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
# Create your views here.


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def user_list(request):
   
    serializer = UserTpvSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def user_address(request):
   
    serializer = UserAddressTpvSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def user_info(request):
   
    serializer = UserInfoEmployees(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def user_signin(request):
   
    pass
    #serializer = UserInfoEmployees(data=request.data)
    #if serializer.is_valid():
    #    serializer.save()
    #    return Response(serializer.data, status=status.HTTP_201_CREATED)
    #return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def add_phone(request):

    serializer = AddDispSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def app_phone(request):

    serializer = AddDispSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def get_phone(request):

    list_phone = AddDisp.objects.all()
    list_phone_serializer = AddDispSerializer(list_phone, many=True)
    return Response(list_phone_serializer.data)

